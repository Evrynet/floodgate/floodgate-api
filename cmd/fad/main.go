package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/market"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/order"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/register"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/settle"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/verify"
)

var (
	bts, rev, version string
	log               = logrus.New()
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	version = fmt.Sprintf("%s::%s", bts, rev)
	log.Info("version = ", version)
	cfg, err := utils.GetConfig()
	if err != nil {
		log.Fatal("Error reading configuration")
	}
	log.Info("uri = ", cfg.APIURI)
	log.Info("wallet = ", cfg.PubKey)
	var token, tmarket, address, price, side, size, basep, quotep, status string
	app := &cli.App{
		Name:  "fad",
		Usage: "floodgate API demo",
		Commands: []*cli.Command{
			{
				Name:    "register",
				Aliases: []string{"r"},
				Usage:   "register with floodgate API server",
				Action: func(c *cli.Context) error {
					return register.Do()
				},
			},
			{
				Name:    "verify",
				Aliases: []string{"v"},
				Usage:   "verify registration with floodgate API server",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "token",
						Usage:       "verification token received by email",
						Required:    true,
						Destination: &token,
					},
				},
				Action: func(c *cli.Context) error {
					log.Info("token = ", token)
					return verify.Do(token)
				},
			},
			{
				Name:    "place-order",
				Aliases: []string{"po"},
				Usage:   "place order",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "market",
						Usage:       "which market do you want to trade?",
						Required:    true,
						Destination: &tmarket,
					},
					&cli.StringFlag{
						Name:        "address",
						Usage:       "market address",
						Required:    true,
						Destination: &address,
					},
					&cli.StringFlag{
						Name:        "price",
						Usage:       "limit order price",
						Required:    true,
						Destination: &price,
					},
					&cli.StringFlag{
						Name:        "side",
						Usage:       "one of: buy, sell",
						Required:    true,
						Destination: &side,
					},
					&cli.StringFlag{
						Name:        "size",
						Usage:       "base asset quantity you wish to trade",
						Required:    true,
						Destination: &size,
					},
				},
				Action: func(c *cli.Context) error {
					return order.Place(tmarket, address, price, side, size)
				},
			},
			{
				Name:    "settle",
				Aliases: []string{"st"},
				Usage:   "create settle",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "market",
						Usage:       "which market do you want to settle?",
						Required:    true,
						Destination: &tmarket,
					},
					&cli.StringFlag{
						Name:        "address",
						Usage:       "market address",
						Required:    true,
						Destination: &address,
					},
				},
				Action: func(c *cli.Context) error {
					return settle.Settle(tmarket, address)
				},
			},
			{
				Name:    "get-markets",
				Aliases: []string{"gms"},
				Usage:   "get markets supported by floodgate API server",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "basep",
						Usage:       "optional base asset pattern",
						Destination: &basep,
					},
					&cli.StringFlag{
						Name:        "quotep",
						Usage:       "optional quote asset pattern",
						Destination: &quotep,
					},
				},
				Action: func(c *cli.Context) error {
					pairs, err := market.GetAll(basep, quotep)
					if err != nil {
						return err
					}
					fmt.Println(pairs)
					return nil
				},
			},
			{
				Name:    "get-market",
				Aliases: []string{"gm"},
				Usage:   "get data for a specific market",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "market",
						Usage:       "market of interest e.g. SOL/USDC",
						Required:    true,
						Destination: &tmarket,
					},
				},
				Action: func(c *cli.Context) error {
					mdata, err := market.Get(tmarket)
					if err != nil {
						return err
					}
					fmt.Println(mdata)
					return nil
				},
			},
			{
				Name:    "get-orders",
				Aliases: []string{"gos"},
				Usage:   "get orders for a specific market",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "address",
						Usage:       "market address",
						Required:    true,
						Destination: &address,
					},
					&cli.StringFlag{
						Name:        "status",
						Usage:       "optional: order status",
						Destination: &status,
					},
				},
				Action: func(c *cli.Context) error {
					odata, err := order.GetAll(address, status)
					if err != nil {
						return err
					}
					fmt.Println(odata)
					return nil
				},
			},
			{
				Name:    "get-order",
				Aliases: []string{"go"},
				Usage:   "get order for a specific id",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "id",
						Usage:       "order id",
						Required:    true,
						Destination: &address,
					},
				},
				Action: func(c *cli.Context) error {
					odata, err := order.Get(address)
					if err != nil {
						return err
					}
					fmt.Println(odata)
					return nil
				},
			},
			{
				Name:    "cancel-order",
				Aliases: []string{"co"},
				Usage:   "cancel order with a specific id",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "id",
						Usage:       "order id",
						Required:    true,
						Destination: &address,
					},
				},
				Action: func(c *cli.Context) error {
					odata, err := order.Cancel(address)
					if err != nil {
						return err
					}
					fmt.Println(odata)
					return nil
				},
			},
		},
	}

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
