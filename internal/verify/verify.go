package verify

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

type Request struct {
	Token string `json:"token"`
}

var log = logrus.New()

func Do(token string) error {
	rq := Request{Token: token}
	return utils.ExecRequest("/register/verify", http.MethodPost, rq, nil)
}
