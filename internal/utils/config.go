package utils

import (
	"fmt"
	"os"
)

type Config struct {
	APIURI  string
	Email   string
	Org     string
	PrivKey string
	PubKey  string
}

func GetConfig() (*Config, error) {
	keys := []string{"FG_API_URI", "FAD_EMAIL", "FAD_ORG", "FAD_PRIVATE_KEY", "FAD_PRIVATE_KEY"}

	for _, k := range keys {
		if os.Getenv(k) == "" {
			err := fmt.Errorf("env var '%s' not set", k)
			log.Error(err)
			return nil, err
		}
	}
	return &Config{
		APIURI:  os.Getenv("FG_API_URI"),
		Email:   os.Getenv("FAD_EMAIL"),
		Org:     os.Getenv("FAD_ORG"),
		PrivKey: os.Getenv("FAD_PRIVATE_KEY"),
		PubKey:  os.Getenv("FAD_PUBLIC_KEY"),
	}, nil
}
