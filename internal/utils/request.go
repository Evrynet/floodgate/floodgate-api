package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type SignatureReq struct {
	ClientId string `json:"client_id,omitempty"`
	TxId     string `json:"tx_id"`
	TxData   string `json:"tx_data"`
	TxType   string `json:"tx_type"`
}

type SignatureData struct {
	Signature string `json:"signature"`
}

func ExecRequest(path, method string, in, out interface{}) error {
	var (
		payload []byte
		err     error
	)
	if in != nil {
		payload, err = json.Marshal(in)
		if err != nil {
			log.Error(err)
			return err
		}
	}
	req, err := SignReq(path, method, payload)
	if err != nil {
		log.Error(err)
		return err
	}
	req.Close = true
	client := http.Client{Timeout: time.Duration(30) * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		bb, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Error(err)
			return err
		}
		bs := string(bb)
		err = fmt.Errorf("API server responded with %d: '%s'", resp.StatusCode, bs)
		log.Error(err)
		return err
	}
	if out != nil {
		err = json.NewDecoder(resp.Body).Decode(&out)
		if err != nil {
			log.Error(err)
			return err
		}

	}
	return nil
}
