package utils

import (
	"bytes"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/btcsuite/btcutil/base58"
	"github.com/sirupsen/logrus"
)

var log = logrus.New()

func Sign(privateKey, payload string) string {
	decodedKey := base58.Decode(privateKey)
	private := ed25519.PrivateKey(decodedKey)
	sig := ed25519.Sign(private, []byte(payload))
	return hex.EncodeToString(sig)
}

func SignReq(path string, method string, body []byte) (*http.Request, error) {
	cfg, err := GetConfig()
	if err != nil {
		log.Error("Error reading configuration")
		return nil, err
	}
	ts := strconv.FormatInt(time.Now().UnixMilli(), 10)

	signaturePayload := ts + method + removeQueryParamsForSign(path) + string(body)
	signature := Sign(cfg.PrivKey, signaturePayload)

	req, err := http.NewRequest(method, cfg.APIURI+path, bytes.NewBuffer(body))
	if err != nil {
		logrus.Errorf("[%s] while creating request to %s", err, path)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-FG-KEY", cfg.PubKey)
	req.Header.Set("X-FG-SIGN", signature)
	req.Header.Set("X-FG-TS", ts)

	return req, nil
}

func removeQueryParamsForSign(path string) string {
	if idx := strings.Index(path, "?"); idx != -1 {
		return path[:idx]
	}
	return path
}

func SignBody(payload string) string {
	cfg, err := GetConfig()
	if err != nil {
		log.Error("Error reading configuration")
		return ""
	}
	decodedKey := base58.Decode(cfg.PrivKey)
	private := ed25519.PrivateKey(decodedKey)
	payloadDecoded, err := base64.StdEncoding.DecodeString(payload)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	sig := ed25519.Sign(private, payloadDecoded)
	return hex.EncodeToString(sig)
}
