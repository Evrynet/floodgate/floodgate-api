package settle

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

type SettleReq struct {
	ClientId      string `json:"client_id"`
	Market        string `json:"market"`
	MarketAddress string `json:"market_address"`
}

type SignedSettles struct {
	Success    bool   `json:"success"`
	ResultType string `json:"result_type"`
}

var log = logrus.New()

func Settle(market, address string) error {
	cfg, err := utils.GetConfig()
	if err != nil {
		log.Error("Error reading configuration")
		return err
	}
	rq := SettleReq{
		ClientId:      cfg.PubKey,
		Market:        market,
		MarketAddress: address,
	}
	srq := utils.SignatureReq{}

	// initiate settle
	log.Info("initiating settle..")
	err = utils.ExecRequest("/settle", http.MethodPost, rq, &srq)
	if err != nil {
		log.Error(err)
		return err
	}

	log.Info("signing settle..")
	res := SignedSettles{}
	sd := utils.SignatureData{}
	sd.Signature = utils.SignBody(srq.TxData)
	err = utils.ExecRequest(fmt.Sprintf("/tx/%s/sign", srq.TxId), http.MethodPost, sd, &res)
	if err != nil {
		log.Error(err)
		return err
	}
	od, _ := json.Marshal(res)
	log.Info("settle: ", string(od))
	return nil
}
