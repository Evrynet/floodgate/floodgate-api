package order

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

func Get(oid string) (string, error) {
	orq := OrderResult{}
	uri := fmt.Sprintf("/orders/%s", oid)
	log.Info("uri = ", uri)
	err := utils.ExecRequest(uri, http.MethodGet, nil, &orq)
	if err != nil {
		log.Error(err)
		return "", err
	}

	ors, err := json.Marshal(orq)
	if err != nil {
		log.Error(err)
		return "", err
	}
	return string(ors), nil
}
