package order

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

func Cancel(oid string) (string, error) {
	uri := fmt.Sprintf("/orders/%s", oid)
	log.Info("[delete] uri = ", uri)
	srq := utils.SignatureReq{}
	// initiate order
	log.Info("initiating cancelation..")
	err := utils.ExecRequest(uri, http.MethodDelete, nil, &srq)
	if err != nil {
		log.Error(err)
		return "", err
	}

	if srq.TxData == "" {
		log.Info("empty solana transaction, we are done")
		return "", nil
	}

	log.Info("signing cancelation transaction..")
	res := SignedOrders{}
	sd := utils.SignatureData{}
	sd.Signature = utils.SignBody(srq.TxData)
	log.Info("returning cancelation transaction..")
	err = utils.ExecRequest(fmt.Sprintf("/tx/%s/sign", srq.TxId), http.MethodPost, sd, &res)
	if err != nil {
		log.Error(err)
		return "", err
	}
	od, _ := json.Marshal(res)
	log.Info("canceled order: ", string(od))
	return string(od), nil
}
