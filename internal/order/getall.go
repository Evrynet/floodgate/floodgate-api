package order

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

type OrderResult struct {
	Orders []Order `json:"orders"`
}

func GetAll(address, status string) (string, error) {
	orq := OrderResult{}
	uri := ""
	if status != "" {
		uri = fmt.Sprintf("/markets/%s/orders?status=%s", address, status)
	} else {
		uri = fmt.Sprintf("/markets/%s/orders", address)
	}
	log.Info("uri = ", uri)
	err := utils.ExecRequest(uri, http.MethodGet, nil, &orq)
	if err != nil {
		log.Error(err)
		return "", err
	}

	ors, err := json.Marshal(orq)
	if err != nil {
		log.Error(err)
		return "", err
	}
	return string(ors), nil
}
