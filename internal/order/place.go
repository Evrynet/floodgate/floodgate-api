package order

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

type PlaceReq struct {
	ClientId      string `json:"client_id"`
	Market        string `json:"market"`
	MarketAddress string `json:"market_address"`
	Price         string `json:"price"`
	Side          string `json:"side"`
	Size          string `json:"size"`
	OrderType     string `json:"order_type"`
}

type Order struct {
	CreatedAt     string `json:"created_at"`
	ModifiedAt    string `json:"modified_at,omitempty"`
	Id            string `json:"id"`
	FilledSize    string `json:"filled_size"`
	ClientId      string `json:"client_id"`
	Market        string `json:"market"`
	MarketAddress string `json:"market_address"`
	Price         string `json:"price"`
	Side          string `json:"side"`
	Size          string `json:"size"`
	OrderType     string `json:"order_type"`
	Status        string `json:"status"`
}

type SignedOrders struct {
	Success    bool    `json:"success"`
	ResultType string  `json:"result_type"`
	Orders     []Order `json:"orders,omitempty"`
}

var log = logrus.New()

func Place(market, address, price, side, size string) error {
	cfg, err := utils.GetConfig()
	if err != nil {
		log.Error("Error reading configuration")
		return err
	}
	rq := PlaceReq{
		ClientId:      cfg.PubKey,
		Market:        market,
		MarketAddress: address,
		Price:         price,
		Side:          side,
		Size:          size,
		OrderType:     "limit",
	}
	srq := utils.SignatureReq{}
	// initiate order
	log.Info("initiating order..")
	err = utils.ExecRequest("/orders", http.MethodPost, rq, &srq)
	if err != nil {
		log.Error(err)
		return err
	}

	log.Info("signing order..")
	res := SignedOrders{}
	sd := utils.SignatureData{}
	sd.Signature = utils.SignBody(srq.TxData)
	err = utils.ExecRequest(fmt.Sprintf("/tx/%s/sign", srq.TxId), http.MethodPost, sd, &res)
	if err != nil {
		log.Error(err)
		return err
	}
	od, _ := json.Marshal(res)
	log.Info("placed order: ", string(od))
	return nil
}
