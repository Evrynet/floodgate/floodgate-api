package register

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

var log = logrus.New()

type Request struct {
	OrgName       string `json:"org_name"`
	Email         string `json:"email"`
	WalletAddress string `json:"wallet_address"`
}

func Do() error {
	cfg, err := utils.GetConfig()
	if err != nil {
		log.Error("Error reading configuration")
		return err
	}
	rq := Request{
		OrgName:       cfg.Org,
		Email:         cfg.Email,
		WalletAddress: cfg.PubKey,
	}
	return utils.ExecRequest("/register", http.MethodPost, rq, nil)
}
