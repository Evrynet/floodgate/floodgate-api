package market

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

var log = logrus.New()

type MarketResult struct {
	Success bool       `json:"success"`
	Market  MarketData `json:"market"`
}
type MarketData struct {
	ProgramId     string `json:"program_id"`
	MarketAddress string `json:"market_address"`
	Name          string `json:"name"`
	Base          string `json:"base"`
	Quote         string `json:"quote"`
	BaseMintAddr  string `json:"base_mint_addr"`
	QuoteMintAddr string `json:"quote_mint_addr"`
	MinOrderSize  string `json:"min_order_size"`
	TickSize      string `json:"tick_size"`
}

func Get(pair string) (string, error) {
	mr := MarketResult{}
	err := utils.ExecRequest(fmt.Sprintf("/markets/%s", pair), http.MethodGet, nil, &mr)
	if err != nil {
		log.Error(err)
		return "", err
	}
	ms, err := json.Marshal(mr)
	if err != nil {
		log.Error(err)
		return "", err
	}
	return string(ms), nil
}
