package market

import (
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/floodgate/floodgate-api/internal/utils"
)

type Pairs struct {
	Success bool     `json:"success"`
	Pairs   []string `json:"pairs"`
}

func GetAll(basep, quotep string) (Pairs, error) {
	pairs := Pairs{}
	marketsURL := "/markets"
	switch {
	case basep != "" && quotep == "":
		marketsURL = fmt.Sprintf("%s?basep=%s", marketsURL, basep)
	case basep == "" && quotep != "":
		marketsURL = fmt.Sprintf("%s?quotep=%s", marketsURL, quotep)
	default:
		marketsURL = fmt.Sprintf("%s?basep=%s&quotep=%s", marketsURL, basep, quotep)
	}

	err := utils.ExecRequest(marketsURL, http.MethodGet, nil, &pairs)
	if err != nil {
		log.Error(err)
	}
	return pairs, err
}
