# floodgate-api

[OpenAPI](https://swagger.io/specification/) specification for the `floodgate` DEX API server + client development libraries & examples

## preliminaries

This repository contains a fully working API demo client that you can examine and play with to get up to speed.

In order to use the example `go` code please

1.  copy the `example.env` file to `.env` and edit the latter as needed
2.  build by running `make`
3.  run the demo client by calling `bin/fad` and see the help output for details

Obviously, you will need

- a `go` compiler and `make` installed on your system to play around :)
- a bit of `SOL` in the wallet to do anything meaningful

### the .env file

In order to use the API server you will need a solana wallet (e.g. [phantom](https://phantom.app/)). The `FAD_PUBLIC_KEY` and `FAD_PRIVATE_KEY` variables hold the wallet address and the private key (in `base58` format!) respectively.

## overview

The API was modeled to be similar to one that a centralized exchange would offer so users would find it easier to learn and adopt.

However, `floodgate` operates on top of a decentralized exchange (DEX) - [serum](https://portal.projectserum.com/) - and there are significant differences to a centralized exchange. Most notably, a client

- uses its solana wallet public / private keys as API key and secret key respectively
- any operation that potentially changes asset balances takes 2 steps

  1. first the client initiates such an operation and receives a response
     containing a solana transaction that encapsulates all the parameters
     and details.
  2. then the client signs the solana transaction returned by the API
     server and posts the signature on a separate API endpoint
     (`/tx/{tx_id}/sign`).

  The second step is required since any changes to assets in the client's
  wallet must be authorized by a signature with the corresponding private
  key.

Please note that the 2-step approach described above has the following benefits: the API client

- never needs to share its private key with the API server
- is not concerned with the nitty-gritty of constructing solana transactions

## registration with the API server

Speaking of 2-step processes :) a client needs to

1. [register](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/register) with the API server
2. [verify](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/verify) its email address (specified in the previous step)

before it can use the API server. Otherwise you will get to see the dreaded `{"code":1002,"message":"Account not yet registered"}` error :)

Please see the demo code in [register.go](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/register/register.go) and [verify.go](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/verify/verify.go) for a working example of how this works.

## authentication

Every `floodgate` API request needs to be authenticated. We use a scheme that is analogous to the [FTX API authentication](https://blog.ftx.com/blog/api-authentication/) i.e. the following headers must be present:

- `X-FG-KEY`: the public key of the solana wallet passed as a base-58 encoded string
- `X-FG-TS`: a time stamp (number of milliseconds since Unix epoch), must not be older than 5 minutes
- `X-FG-SIGN`: a [`ed25519` signature](https://docs.solana.com/developing/programming-model/transactions#signature-format) (in [hexadecimal encoding](https://pkg.go.dev/encoding/hex)) of the following strings:
  1. request timestamp (same as in `X-FG-TS`)
  1. HTTP method in uppercase (e.g. GET or POST)
  1. request path, including leading slash and any URL parameters but not including the hostname (e.g. /account)
  1. request body (JSON-encoded) only for POST requests

Please see the functions in [sign.go](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/utils/sign.go) for a working example of how this works.

## getting the list of markets

The floodgate API server supports a number of markets and you probably want to [get this list](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/getMarkets) first.

[This example code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/market/getall.go) shows how to use the API and get these.

If it works you should be seeing something like this:

```
{true [SOL/USDC BTC/USDC BTC/USDT SRM/USDC SOL/USDT MSOL/USDC ETH/USDC ATLAS/USDC AVAX/USDC ETH/SOL SUSHI/USDC LUNA/USDC USDT/USDC soETH/USDC VOID/SOL LINK/USDC RAY/USDC SAMO/RAY renDOGE/USDC POLIS/USDC BNB/USDC ATLAS/RAY RAY/SRM mSOL/USDT LSTAR/USDC MATICpo/USDC DUST/USDC DUST/SOL FTT/USDC SRM/SOL ETH/USDT SRM/USDT RAY/USDT FTT/USDT LINK/USDT MSOL/SOL]}
```

## getting data for a specific market

In order to trade you will need to have a market's address as well as the minimum order and the tick size.

You can use [this endpoint](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/getMarketForPair) to get that and more. Again, here's [some sample code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/market/get.go) to show you how.

The output you should see is something like

```json
{
  "success": true,
  "market": {
    "program_id": "9xQeWvG816bUx9EPjHmaT23yvVM2ZWbrrpZb9PusVFin",
    "market_address": "9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT",
    "name": "SOL/USDC",
    "base": "SOL",
    "quote": "USDC",
    "base_mint_addr": "So11111111111111111111111111111111111111112",
    "quote_mint_addr": "EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v",
    "min_order_size": "0.1",
    "tick_size": "0.001"
  }
}
```

## placing orders

Placing an order happens in 2 steps as described in the [overview](#overview).
In order for this to work you need some SOL in your wallet. If you intend to place an order that sells `0.1` SOL for USDC your wallet balance must be bigger than `0.1` to pay for the cost of placing the order.

If you see the following error you don't have enough SOL in your wallet:

```
"Transaction simulation failed: Error processing Instruction 1: custom program error: 0x1"
```

In case of success you should see something similar to

```json
{
  "success": true,
  "result_type": "SINGLE_ORDER",
  "orders": [
    {
      "created_at": "2022-05-26T05:11:56.432Z",
      "modified_at": "2022-05-26T05:12:02.281Z",
      "id": "18640ebe-6718-4cd6-a818-62f3a680cc28",
      "order_id": "628f0c1c",
      "filled_size": "0",
      "client_id": "2LRVNJj2hJGkqsPgKZMs9YjHhpgxRofdNPtjGxV3isBC",
      "market": "SOL/USDC",
      "market_address": "9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT",
      "price": "1853",
      "side": "sell",
      "size": "0.1",
      "order_type": "limit",
      "status": "OPEN"
    }
  ]
}
```

### order states

Please note that the status of the new order is `OPEN`. It means the transaction was sent to the solana network and the order was added to the serum order book. Other possible status values are:

- new: the order was added to the floodgate API server database but the corresponding transaction has not been sent to the solana network yet
- timed out: we sent the transaction to the solana network but timed out while awaiting the confirmation; this status will change to `OPEN` or `FAILED` eventually
- failed: we sent the transaction to the solana network but it failed to execute -- this most likely indicates a problem/bug in the floodgate API server logic
- canceled: the user canceled the order
- closed: the order was filled

### step 1: initiate the order placement

We first use [this API endpoint](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/placeOrder) to [initiate the placement of the order](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/place.go#L63) and pass all the parameters to the API server.

If that is successful the API server responds with a [signature request](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/utils/request.go#L11) where the `tx_data` property holds the actual `solana` transaction we need to sign and pass back in the next step.

Please note that the signature request data is stored in the [`srq` struct in the example code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/place.go#L60).

### step 2: sign the solana transaction and pass it back

We [sign the solana transaction](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/place.go#L72) with our private key and then [pass the signature back to the API server](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/place.go#L73) using [this API endpoint](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/signTX).

This allows the API server to forward the actual [solana transaction](https://docs.solana.com/developing/programming-model/transactions) that creates the order (in the serum DEX order book) to the solana network.

## getting orders

There's two ways to access existing orders:

1.  [getting all orders for a given market](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/getMarketOrders), optionally filtered by order status ([example code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/getall.go))
2.  [getting a single order](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/getOrder) based on the `order_id` ([example code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/get.go))

## canceling orders

As explained in the [order states](#order-states) section above an order placed by you may end up being in a different state depending on what is going on :)

Only orders in one of the following states may be canceled (via the [DELETE `/orders/{id}`](https://app.swaggerhub.com/apis/c653/floodgate/1.0.0#/default/cancelOrder) endpoint): `new`, `timed_out` or `open`:

- `new` if an order is in this state it means it only exists in the floodgate API server internal database and canceling it will remove it from the database in a single step
- `timed_out` or `open`: if in either of these states the API server will prepare a solana transaction that also removes the order in question from the serum DEX order book and return that transaction in the response. The client needs to sign it and return the signature (using the `/tx/{tx_id}/sign` endpoint) to the API server in a 2-step process.

Please see [this code](https://gitlab.com/Evrynet/floodgate/floodgate-api/-/blob/master/internal/order/cancel.go) for a working example of how to cancel an order.

If it all works you should see something like

```json
{
  "success": true,
  "result_type": "SINGLE_CANCEL",
  "orders": [
    {
      "created_at": "2022-05-26T05:11:56.432Z",
      "modified_at": "2022-05-26T05:12:02.281Z",
      "id": "18640ebe-6718-4cd6-a818-62f3a680cc28",
      "filled_size": "0",
      "client_id": "2LRVNJj2hJGkqsPgKZMs9YjHhpgxRofdNPtjGxV3isBC",
      "market": "SOL/USDC",
      "market_address": "9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT",
      "price": "1853",
      "side": "sell",
      "size": "0.1",
      "order_type": "limit",
      "status": "CANCELED"
    }
  ]
}
```

## dev endpoints

### `GET /dev/keypair`

This will return a unique keypair that contains a `publicKey` and a `secretKey`. Developers can use them with other endpoints after they register it

Please note:

- `publicKey`: base58 publicKey, can be used as a wallet address
- `secretKey`: base64 secretKey, to be used with other dev endpoints

In case of success you should see something similar to

```json
{
  "publicKey": "oiSZav4rHUmj8nujFEuf4wmwkFt7DkZJsurDBKqd8T1",
  "secretKey": "Q7IjguGeGY/0Y/fYQ+kmSxciAIkIrtPaHysAJ8U7Ja8L95MLNkydzdePM5QPzpC9DUIPVpcf23tYmLLMqLFtQA=="
}
```

### `GET /dev/time`

Return the current timestamp of the server in milliseconds (ms)

In case of success you should see something similar to

```json
{
  "success": true,
  "timestamp": 1654654088634
}
```

### `POST /dev/headers`

#### Create required headers for a request based on provided input

Please note:

- while this is useful for developers to see example headers accepted by the server, this endpoint requires a wallet `secretKey`
- please be cautious and use the `secretKey` generated with `GET /dev/keypair`
- `/dev/headers` request body overview

```
{
   "method": Request method, case sensitive. e.g. "GET", "POST", "DELETE",...
   "endpoint": Request endpoint, start with a "/". e.g. "/markets"
   "secretKey": base64 secretKey. This will be used to sign the payload and create the signature
   "message": Optional. JSON stringify version of request body. Can be omitted if the request body is empty
   "timestamp": Optional. Timestamp in milliseconds (ms). Will return current timestamp if not provided
}
```

- Example of creating headers of a request without body

```json
{
  "method": "GET",
  "endpoint": "/markets",
  "secretKey": "Q7IjguGeGY/0Y/fYQ+kmSxciAIkIrtPaHysAJ8U7Ja8L95MLNkydzdePM5QPzpC9DUIPVpcf23tYmLLMqLFtQA=="
}
```

In case of success you should see something similar to

```json
{
  "publicKey": "oiSZav4rHUmj8nujFEuf4wmwkFt7DkZJsurDBKqd8T1",
  "timestamp": 1654656306161,
  "signature": "ecee2be1812e98d784e08404f68831cf4dcde19e9536a04142a2d6ab1c4bd0fa59c64ba6571be0a811347c8233a641c39cce978d92ba16b091a120e2b78c710f"
}
```

- Example of creating headers of a request with body

```json
{
  "method": "POST",
  "endpoint": "/register",
  "secretKey": "Q7IjguGeGY/0Y/fYQ+kmSxciAIkIrtPaHysAJ8U7Ja8L95MLNkydzdePM5QPzpC9DUIPVpcf23tYmLLMqLFtQA==",
  "message": "{\"email\":\"testing@test.com\",\"org_name\":\"test-org-name\",\"wallet_address\":\"oiSZav4rHUmj8nujFEuf4wmwkFt7DkZJsurDBKqd8T1\"}"
}
```

In case of success you should see something similar to

```json
{
  "publicKey": "oiSZav4rHUmj8nujFEuf4wmwkFt7DkZJsurDBKqd8T1",
  "timestamp": 1654656058623,
  "signature": "a31f76c0e2baa9976630fd637975628e9e9255874dd358a1c6098cfdbaaf3af55449196a1d5e212141e52bd40c579018a17462c1b9daa73ba28a338f3caa6209"
}
```

### `POST /dev/sign`

#### Sign the payload with the provided secretKey

This is typically used to sign a transaction and create the corresponding signature. Please note:

- Similar to `POST /dev/headers`, you are required to provide a wallet `secretKey`
- please be cautious and use the `secretKey` generated with `GET /dev/keypair`
- This endpoint is a reference showing what the signature should look like. Using this to manually to sign a transaction will probably take too long (`blockhash` expired)
- `/dev/sign` request body overview

```
{
   "message": base64 message. This is returned from server when creating a transaction
   "secretKey": base64 secretKey. This will be used to sign the payload and create the signature
}
```

e.g. Calling `POST /orders` successfully will return something similar to

```json
{
  "client_id": "3fdUY9A9hERgDGEeNAMBuQ9j2qJFuGdRc9kMj3A4MawY",
  "tx_id": "b51882bb-af52-41be-b54d-fe5483b6b6ac",
  "tx_data": "AQADDSecr89C6lWm+Ekev21fwxamYsUBI/Yhdq7MsQdhtOJvPXPhuUYjfeO2mm0XxC2NkX2t9AHBBLzm+k60iJKU6Ow+EfoZVSIZO6HZT9JJoy1mxV5/zHz5WOITYtXegCSTykw06Wj6nhza1AaAXu8RTpL1jCdvfPe4KKJbrwu6F6w7Y17pFjnaQZd2S4loq92bBHYbgbisspua8JAB6DCYBK9sQo8TCu4I1+xzpdAAPuf4cw2MVroUbcruTKgb5/qO04Z80Gpyzy+9G6hm+LBDl9BX+Gvr758ni++L+LouT90A2kfEwFHBZ1Y6eFbzox37ks3nuhiI5Jj8CPU0xn6qsA/u2fKIzizABkvhruNa9K7mGCO9VghHiKM9TfWiAR7gJQvZ7+rYC8h87FeNu6wNeOA474XrLb/GY21oai36Wuaqtb2ey2edNhrfMG/1i9HcqzkuHskmz5zohZVVAY7FbR8Gp9UXGSxcUSGMyUw9SvF/WNruCJuh/UTj29mKAAAAAAbd9uHXZaGT2cvhRs7reawctIXtX1s3kTqM9YV+/wCpqbOWZV+lqnXk49WBIrqZGKGDwh2uUmwJzWUJVFIeduQDCgcGCQIDBAgFBwACAAAABQAKDAYHCQIDBAEACAUMCzMACgAAAAAAAABkAAAAAAAAAAEAAAAAAAAAAMqaOwAAAAAAAAAAAAAAAFtMoWIAAAAA//8KBwYJAgMECAUHAAIAAAAFAA==",
  "tx_type": "SINGLE_ORDER"
}
```

We will take the `tx_data` and provide it to `POST /dev/sign` to create the signature

```json
{
  "message": "AQADDSecr89C6lWm+Ekev21fwxamYsUBI/Yhdq7MsQdhtOJvPXPhuUYjfeO2mm0XxC2NkX2t9AHBBLzm+k60iJKU6Ow+EfoZVSIZO6HZT9JJoy1mxV5/zHz5WOITYtXegCSTykw06Wj6nhza1AaAXu8RTpL1jCdvfPe4KKJbrwu6F6w7Y17pFjnaQZd2S4loq92bBHYbgbisspua8JAB6DCYBK9sQo8TCu4I1+xzpdAAPuf4cw2MVroUbcruTKgb5/qO04Z80Gpyzy+9G6hm+LBDl9BX+Gvr758ni++L+LouT90A2kfEwFHBZ1Y6eFbzox37ks3nuhiI5Jj8CPU0xn6qsA/u2fKIzizABkvhruNa9K7mGCO9VghHiKM9TfWiAR7gJQvZ7+rYC8h87FeNu6wNeOA474XrLb/GY21oai36Wuaqtb2ey2edNhrfMG/1i9HcqzkuHskmz5zohZVVAY7FbR8Gp9UXGSxcUSGMyUw9SvF/WNruCJuh/UTj29mKAAAAAAbd9uHXZaGT2cvhRs7reawctIXtX1s3kTqM9YV+/wCpqbOWZV+lqnXk49WBIrqZGKGDwh2uUmwJzWUJVFIeduQDCgcGCQIDBAgFBwACAAAABQAKDAYHCQIDBAEACAUMCzMACgAAAAAAAABkAAAAAAAAAAEAAAAAAAAAAMqaOwAAAAAAAAAAAAAAAFtMoWIAAAAA//8KBwYJAgMECAUHAAIAAAAFAA==",
  "secretKey": "Q7IjguGeGY/0Y/fYQ+kmSxciAIkIrtPaHysAJ8U7Ja8L95MLNkydzdePM5QPzpC9DUIPVpcf23tYmLLMqLFtQA=="
}
```

In case of success you should see something similar to

```json
{
  "publicKey": "oiSZav4rHUmj8nujFEuf4wmwkFt7DkZJsurDBKqd8T1",
  "secretKey": "Q7IjguGeGY/0Y/fYQ+kmSxciAIkIrtPaHysAJ8U7Ja8L95MLNkydzdePM5QPzpC9DUIPVpcf23tYmLLMqLFtQA==",
  "signature": "d91c694b273abe85027a3931ba4b704b9793524fb4565a080679bd4ac3e762b022498ff42197f8041c6b1c294e285d58a5c47e7d97072b7704a61ec77a372b07"
}
```

The signature can be used with `POST tx/:tx_id/sign` so the transaction can be sent.
