module gitlab.com/Evrynet/floodgate/floodgate-api

go 1.18

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/joho/godotenv v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.8.0
)

require (
	github.com/antzucaro/matchr v0.0.0-20210222213004-b04723ef80f0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	golang.org/x/sys v0.0.0-20220702020025-31831981b65f // indirect
)
